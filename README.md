This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel
Install prisma
```bash
npm install prisma --save-dev
npx prisma init
```
Create acount on [Vercel Platform](https://vercel.com/).
* from the nav bar select "Storage".
* create a new database (choose "PostgreSQL").
* copy prisma datasource parameter and change the existed one in schema.prisma file.
* copy .env.local parameter and change the DB_URL in .env.local file.

### define your model(table) in schema.prisma file and run migrations.

```bash
npx prisma migrate dev
```
if you get an error try to change timeout parameter in POSTGRES_PRISMA_URL in .env file.

*open Prisma studio to check the database and tables.*
```bash
npx prisma studio
```

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
